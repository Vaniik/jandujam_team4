﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Ice : MonoBehaviour {

    [SerializeField] GameObject Ball1;
    [SerializeField] GameObject Ball2;
    GameObject Block;

    void Start()
    {
        Block = GameObject.Find("IceBlock");
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "Ball1" && Ball1.GetComponent<Ball>().element == "Fire")
        {
            Block.gameObject.SetActive(false);
        }
        if (col.gameObject.name == "Ball2" && Ball2.GetComponent<Ball>().element == "Ice")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
    }
