﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {
    GameObject ball1;
    Rigidbody ball1RB;
    GameObject ball2;
    Rigidbody ball2RB;
    [SerializeField] Vector2 speed;
    [SerializeField] float jumpForce;
    float jumpForce2;
    bool jumpKeyPressed;
    [SerializeField] Vector2 direction;
    bool grounded;
    // Use this for initialization
    void Start () {
        ball1 = transform.GetChild(0).gameObject;
        ball1RB = ball1.GetComponent<Rigidbody>();
        ball2 = transform.GetChild(1).gameObject;
        ball2RB = ball2.GetComponent<Rigidbody>();
        jumpForce2 = 5;
        jumpKeyPressed = false;
        grounded = true;
        direction = new Vector2(1, 0);
    }
	
	// Update is called once per frame
	void Update () {
        transform.position += new Vector3((speed.x / 100) * direction.x, (speed.y / 100) * direction.y, 0);
        UpdateControls();
	}

    private void FixedUpdate()
    {
        ball2RB.useGravity = false;
        ball2RB.AddForce(0, (Physics.gravity.y * ball2RB.mass * -1), 0);
    }

    private void UpdateControls()
    {
        if (Input.GetKeyDown(KeyCode.Space) && grounded)
        {
            ball1RB.AddForce(new Vector3(0, jumpForce, 0));
            ball2RB.AddForce(new Vector3(0, -jumpForce, 0));
            grounded = false;
            jumpKeyPressed = true;
        }
        if (Input.GetKey(KeyCode.Space) && !grounded && jumpForce2 > 0 && jumpKeyPressed)
        {
            ball1RB.AddForce(new Vector3(0, jumpForce2, 0));
            ball2RB.AddForce(new Vector3(0, -jumpForce2, 0));
            jumpForce2 -= Time.deltaTime;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            jumpKeyPressed = false;
            jumpForce2 = 5;
        }

    }

    public void SetGrounded(bool _grounded)
    {
        grounded = _grounded;
    }

    public bool GetGrounded()
    {
        return grounded;
    }

    public void SwapElements()
    {
        switch (ball1.GetComponent<Ball>().element)
        {
            case "Fire":
                ball1.GetComponent<Ball>().element = "Ice";
                ball2.GetComponent<Ball>().element = "Fire";
                break;
            case "Ice":
                ball1.GetComponent<Ball>().element = "Fire";
                ball2.GetComponent<Ball>().element = "Ice";
                break;
        }
    }
}
