﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {
    public PlayerManager pm;
    public string element;

    public bool colBall;

    float timeCutre;

    private void Start()
    {
        pm = GetComponentInParent<PlayerManager>();
        timeCutre = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Floor" && !pm.GetGrounded())
        {
            pm.SetGrounded(true);
        }
    }
}
