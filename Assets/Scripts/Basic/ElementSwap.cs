﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementSwap : MonoBehaviour {
    [SerializeField] GameObject otherBall;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Ball1")
        {
            GetComponent<Ball>().pm.SwapElements();
            foreach (var contact in collision.contacts)
            {
                contact.otherCollider.GetComponent<Rigidbody>().AddForce(-1 * new Vector3(contact.normal.x, contact.normal.y, 0) * 9, ForceMode.VelocityChange);
                contact.thisCollider.GetComponent<Rigidbody>().AddForce(new Vector3(contact.normal.x, contact.normal.y, 0) * 9, ForceMode.VelocityChange);
            }
        }
    }
}
